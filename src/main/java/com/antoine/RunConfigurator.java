package com.antoine;

import com.antoine.ui.ConfiguratorUI;

import javax.swing.*;

public class RunConfigurator {

    public static void main(String[] arg) {

        SwingUtilities.invokeLater(() -> new ConfiguratorUI());

    }
}

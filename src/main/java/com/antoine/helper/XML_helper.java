package com.antoine.helper;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.Schema;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class XML_helper {

    private String FILE_NAME = "conf.xml";
    private DocumentType docType;
    private Document document;
    private Element root;

    public XML_helper() {

    }

    public void parse(String filePath) throws ParserConfigurationException, IOException, SAXException {
        FILE_NAME = filePath;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        document = builder.parse(filePath);

        docType = document.getDoctype();

        root = document.getDocumentElement();
    }

    public void write(Map<String, HashMap<String, String>> id_values) throws TransformerException {
        Set<String> ids = id_values.keySet();

        for (String id: ids) {
            Element target = document.getElementById(id);
            Map<String, String> attr_values = id_values.get(id);
            Set<String> attributes = attr_values.keySet();

            for (String attribute : attributes)
            {
                target.setAttribute(attribute, attr_values.get(attribute));
            }
        }

        DOMSource source = new DOMSource(document);
        StreamResult output = new StreamResult(new File(FILE_NAME));

        Transformer transformer = TransformerFactory.newInstance().newTransformer();

        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");

        transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, docType.getSystemId());

        transformer.transform(source, output);

    }
}

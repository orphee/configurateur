package com.antoine.ui;


import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.HashMap;
import java.util.Hashtable;

public class SliderPane extends JPanel implements ChangeListener {

    private JSlider slider;
    private JLabel info;
    private String title;
    private JPanel cardPane;
    private HashMap<Integer, String> cardPane_titles;

    private SliderPane(){
        super(new BorderLayout());
        slider = new JSlider();
        super.add(slider, BorderLayout.CENTER);
    }

    public SliderPane(String title){
        super();
        this.title = title;
    }

    public static Builder builder(){
        return new Builder();
    }

    public String getTitle() {
        return title;
    }

    private void createInfo(String info) {
        this.info = new JLabel(info);
        super.add(this.info, BorderLayout.NORTH);
    }

    @Override
    public Dimension getPreferredSize(){
        return super.getPreferredSize();
    }

    public void setValue(int value) {
        slider.setValue(value);
        slider.repaint();
    }

    public int getValue() {
        return slider.getValue();
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        int sliderValue = slider.getValue();

        CardLayout card = (CardLayout) cardPane.getLayout();
        if (cardPane_titles.containsKey(sliderValue)) {
            cardPane.setVisible(true);
            card.show(cardPane, cardPane_titles.get(sliderValue));
        }else
            cardPane.setVisible(false);
        this.repaint();
    }


    public static class Builder{

        private SliderPane pane;

        private Builder(){
            pane = new SliderPane();
        }

        public Builder setTitle(String title){
            pane.title = title;
            return this;
        }

        public Builder setMinValue(int minValue){
            pane.slider.setMinimum(minValue);
            return this;
        }

        public Builder setMaxValue(int maxValue){
            pane.slider.setMaximum(maxValue);
            return this;
        }

        public Builder setMajorTicksGap(int majorTicksGap){
            pane.slider.setMajorTickSpacing(majorTicksGap);
            return this;
        }

        public Builder setMinorTicksGap(int minorTicksGap){
            pane.slider.setMinorTickSpacing(minorTicksGap);
            return this;
        }

        public Builder setPaintTick(boolean paintTick){
            pane.slider.setPaintTicks(paintTick);
            return this;
        }

        public Builder setPaintTrack(boolean paintTrack){
            pane.slider.setPaintTrack(paintTrack);
            return this;
        }

        public Builder setInfo(String info){
            pane.createInfo(info);
            return this;
        }

        public Builder setPaintLabel(boolean paintLabel){
            pane.slider.setPaintLabels(true);
            return this;
        }

        public Builder setValue(int value){
            pane.slider.setValue(value);
            return this;
        }

        public Builder addValueDescriptionPane(){
            pane.cardPane = new JPanel(new CardLayout());
            pane.cardPane.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED), BorderFactory.createBevelBorder(BevelBorder.RAISED)));
            pane.add(pane.cardPane, BorderLayout.SOUTH);
            pane.slider.addChangeListener(pane);
            pane.cardPane_titles = new HashMap<>();
            return this;
        }

        public Builder addDescriptionofValue(int value, String title, JComponent description){
            pane.cardPane.add(title, description);
            pane.cardPane_titles.put(value, title);
            return this;
        }

        public Builder setLabelTable(String[] labels, int increment){

            Hashtable<Integer, JComponent> table = new Hashtable<>();
            int start = pane.slider.getMinimum();
            int max = pane.slider.getMaximum();

            for (String s:labels)
            {
                    table.put(start, new JLabel(s));
                    start += increment;
            }

            pane.slider.setLabelTable(table);

            return this;
        }

        public SliderPane build(){
            if (pane.cardPane != null){
                pane.stateChanged(null);
            }
            return pane;
        }
    }
}

package com.antoine.ui;

import com.antoine.helper.IOHelper;
import com.antoine.helper.XML_helper;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;

public class ConfiguratorUI {

    private final int DISCORD_SPEED = 2;
    private final double THRESHOLD_FACE_DETECTOR = 0.30;

    private String programmeLocation;
    private String configCam_filePath;
    private String configLabyrinthe_filePath;
    private SliderPane threshold_faceDetector_slider, Discord_speed_slider;
    private JFrame mainframe;

    public ConfiguratorUI()
    {
        initPaths();
        initFrame();
    }

    private void createListener(JButton apply, JButton reset)
    {
        reset.addActionListener(
                event-> {
                    threshold_faceDetector_slider.setValue((int) (THRESHOLD_FACE_DETECTOR * 10));

                    Discord_speed_slider.setValue(DISCORD_SPEED);

                    mainframe.repaint();
                }
        );

        apply.addActionListener(event->{
            XML_helper writer = new XML_helper();
            String[] ids = {"levelTwilight_boss_deplacement_vector_deplacementX",
                    "levelTwilight_boss_deplacement_vector_deplacementY"};
            String attributeName = "value";
            String value = "" + Discord_speed_slider.getValue();
            HashMap<String, String> attributes_value = new HashMap<>();
            HashMap<String, HashMap<String, String>> id_attributesNames = new HashMap<>();
            attributes_value.put(attributeName, value);
            for (String id : ids)
            {
                id_attributesNames.put(id, attributes_value);
            }
            try {
                writer.parse(configLabyrinthe_filePath);
                writer.write(id_attributesNames);

                IOHelper.writeFile(new File(configCam_filePath), "" + ((double)threshold_faceDetector_slider.getValue()) / 10);

            } catch (Throwable t){
                throw new RuntimeException(t);
            }finally {
                mainframe.dispose();
            }
        });
    }

    private void initFrame()
    {
        mainframe = new JFrame("Configurator");
        JTabbedPane tabbedPane = new JTabbedPane();
        JButton applyButton = new JButton("apply");
        JButton resetButton = new JButton("reset");

        createListener(applyButton, resetButton);

        JPanel tablePanel = new JPanel();
        JPanel buttonPane = new JPanel();

        threshold_faceDetector_slider = SliderPane.builder()
                .setTitle("FaceRecognizer")
                .setInfo("Choisissez la valeur du seuil de détection de la reconnaissance faciale")
                .setMinValue(2)
                .setMaxValue(6)
                .setLabelTable(new String[] {"detector", "délit de faciès", "myope", "astygmate", "taupe"}, 1)
                .setValue((int) (THRESHOLD_FACE_DETECTOR * 10))
                .addValueDescriptionPane()
                .addDescriptionofValue(2, "detector", new JLabel("<html><br>La position du visage et" +
                        "<br>son expression doivent être la même que sur votre photo de profil.<br>Peu ne pas vous reconnaître!</html>"))
                .addDescriptionofValue(3, "délit de faciès", new JLabel("<html><br>La reconnaissance est plus lente<br>" +
                        "mais plus fine, moins d'erreur</html>"))
                .addDescriptionofValue(4, "myope", new JLabel("Niveau de detection moyenne"))
                .addDescriptionofValue(5, "astygmate", new JLabel("Peu prendre un visage pour un autre"))
                .addDescriptionofValue(6, "taupe", new JLabel("Prendrai votre pied pour votre visage"))
                .setMajorTicksGap(10)
                .setMinorTicksGap(1)
                .setPaintLabel(true)
                .setPaintTick(true)
                .setPaintTrack(true)
                .build();


        Discord_speed_slider = SliderPane.builder()
                .setTitle("Labyrinthe")
                .setInfo("Entrez la vitesse de déplacement de Discord dans le dernier labyrinthe")
                .setMinValue(1)
                .setMaxValue(4)
                .setLabelTable(new String[]{"très lent", "lent", "rapide", "très rapide", "très rapide"}, 1)
                .setValue(DISCORD_SPEED)
                .setMajorTicksGap(1)
                .setPaintLabel(true)
                .setPaintTick(true)
                .setPaintTrack(true)
                .build();

        buttonPane.add(applyButton);
        buttonPane.add(resetButton);

        tabbedPane.add(threshold_faceDetector_slider.getTitle(), threshold_faceDetector_slider);
        tabbedPane.add(Discord_speed_slider.getTitle(), Discord_speed_slider);

        tablePanel.add(tabbedPane);

        Container c = mainframe.getContentPane();
        c.setLayout(new BorderLayout());
        c.add(tablePanel, BorderLayout.CENTER);
        c.add(buttonPane, BorderLayout.SOUTH);

        mainframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainframe.pack();
        mainframe.setResizable(false);
        mainframe.setLocationRelativeTo(null);
        mainframe.setVisible(true);
    }

    private void initPaths() {
        programmeLocation = Paths.get(getClass().getProtectionDomain().getCodeSource().getLocation().getPath()).getParent().toString();

        configCam_filePath = Paths.get(programmeLocation, "config", "config_cam", "conf.txt").toString();

        configLabyrinthe_filePath = Paths.get(programmeLocation, "config", "config_labyrinthe", "conf.xml").toString();
    }
}
